# Stub tests
These data-driven tests are collected executed by the `mypy.test.data` pytest plugin.
The test suite is declared in `test_stubtest.py`.

Note that the structure of this directory must be compatible with what `mypy.test.data` expects.